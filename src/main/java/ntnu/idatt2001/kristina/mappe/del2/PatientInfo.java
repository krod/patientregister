package ntnu.idatt2001.kristina.mappe.del2;

/**
 * Singleton class
 * To get be able to acsess the current patient no matter the scene.
 */
public class PatientInfo {
    /**
     * This object holds the current patient
     */
    private static Patient patient = null;

    /**
     * A constructor that hinders creation of objects of this class
     */
    private PatientInfo(){
        throw new IllegalStateException("This class cannot be instantiated");
    }

    /**
     * Method that can be used anywhere to get the current patient
     */
    public static Patient getPatient(){
        return patient;
    }


    /**
     * Sets the new current patient
     * @param newPatient The new current patient
     */
    public static void setPatient(Patient newPatient){
        patient = newPatient;
    }

    /**
     * Removes the current patient
     */
    public static void removeCurrentPatient(){
        patient = null;
    }

}
