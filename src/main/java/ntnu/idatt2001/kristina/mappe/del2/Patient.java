package ntnu.idatt2001.kristina.mappe.del2;

/**
 * A class that represents a patient.
 */
public class Patient {
    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPractitioner;

    /**
     * A patient constructor that validates the arguments.
     * @param socialSecurityNumber Sets social security number if argument only containts digits of length 11
     * @param firstName Sets first name if argument only contains letters.
     * @param lastName Sets last name if argument only contains letters.
     */
    public Patient(String socialSecurityNumber, String firstName, String lastName) {
        //Social security number
        if(!socialSecurityNumber.matches("[0-9]+")) {
            throw new IllegalArgumentException("Please only enter numbers 0-9 in you social security number.");
        }
        else if(socialSecurityNumber.length() != 11) {
            throw new IllegalArgumentException("Please enter a social security number on the form DDMMYYFFFFF.");
        }
        else {
            this.socialSecurityNumber = socialSecurityNumber;
        }

        //First name
        this.firstName = firstName;

        //Last name
        if(!lastName.chars().allMatch(Character::isLetter)) {
            throw new IllegalArgumentException("The last name you entered does not only contain letters. Please only " +
                    "enter letters in the last name field.");
        }
        else {
            this.lastName = lastName;
        }
    }

    //Accessor and mutator methods
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     *
     * @param socialSecurityNumber The social security number of the patient.
     * @throws IllegalArgumentException if social security number does not only contain digits or is not on the correct form.
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) throws IllegalArgumentException{
        if(!socialSecurityNumber.matches("[0-9]+")) {
            throw new IllegalArgumentException("Please only enter digits 0-9 in you social security number.");
        }
        else if(socialSecurityNumber.length() != 11) {
            throw new IllegalArgumentException("Please enter a social security number on the form DDMMYYFFFFF.");
        }
        else {
            this.socialSecurityNumber = socialSecurityNumber;
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName The last name of the patient
     * @throws IllegalArgumentException when last name do not only conatin letters.
     */
    public void setLastName(String lastName) throws IllegalArgumentException {
        if(!lastName.chars().allMatch(Character::isLetter)) {
            throw new IllegalArgumentException("The last name you entered does not only contain letters. Please only " +
                    "enter letters in the last name field.");
        }
        else {
            this.lastName = lastName;
        }
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    @Override
    public String toString() {
        return "Name: " + getFullName() +
                "\nSocial security number: " + getSocialSecurityNumber() +
                "\nDiagnosis: " + getDiagnosis() +
                "\nGeneral practitioner: " + getGeneralPractitioner();
    }
}
