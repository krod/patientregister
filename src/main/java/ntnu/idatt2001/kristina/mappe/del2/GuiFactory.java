package ntnu.idatt2001.kristina.mappe.del2;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 * A factory class theat creates different types of Nodes. Because fxml and SceneBuilder was used for this program,
 * this class is not actually in use.
 */
public class GuiFactory {
    public static Node create(String nodeType) {
        if (nodeType.isBlank()) {
            return null;
        }
        else if (nodeType.equalsIgnoreCase("VBOX")) {
            return new VBox();
        }
        else if (nodeType.equalsIgnoreCase("TABLEVIEW")) {
            return new TableView<>();
        }
        else if (nodeType.equalsIgnoreCase("MENUBAR")) {
            return new MenuBar();
        }
        else if (nodeType.equalsIgnoreCase("BUTTON")) {
            return new Button();
        }
        else if (nodeType.equalsIgnoreCase("TOOLBAR")) {
            return new ToolBar();
        }
        else if (nodeType.equalsIgnoreCase("IMAGEVIEW")) {
            return new ImageView();
        }
        return null;
    }
}
