package ntnu.idatt2001.kristina.mappe.del2;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Scanner;

public class PrimaryController implements Initializable {

    //Table variables
    @FXML private TableView<Patient> patientTable;
    @FXML private TableColumn<Patient, String> firstNameColumn;
    @FXML private TableColumn<Patient, String> lastNameColumn;
    @FXML private TableColumn<Patient, String> socialSecurityNumberColumn;
    @FXML private TableColumn<Patient, String> diagnosisColumn;
    @FXML private TableColumn<Patient, String> generalPractitionerColumn;

    @FXML private MenuBar menuBar;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //Sets table columns
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        diagnosisColumn.setCellValueFactory(new PropertyValueFactory<>("diagnosis"));
        generalPractitionerColumn.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));
    }

    /**
     * Initiates the input box and adds a new patient to the patient table.
     * @throws IllegalArgumentException if unexcpected input for patient instantiation
     */
    @FXML
    public void addPatientButtonClicked() throws IllegalArgumentException {
        try {
            Parent inputParent = FXMLLoader.load(getClass().getResource("input_box.fxml"));
            Scene inputScene = new Scene(inputParent);
            Stage inputStage = new Stage();
            inputStage.setScene(inputScene);
            inputStage.initModality(Modality.APPLICATION_MODAL);
            inputStage.setTitle("Add patient");
            inputStage.getIcons().add(new Image("registerIcon.png"));
            inputStage.showAndWait();

            patientTable.getItems().add(PatientInfo.getPatient());
            PatientInfo.removeCurrentPatient();
        }

        catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * A method that edits the patient info and table will be updated accordingly.
     * @throws NullPointerException if no patient is selected
     */
    @FXML
    public void editPatientButtonClicked() throws NullPointerException {
        try {
            PatientInfo.setPatient(patientTable.getSelectionModel().getSelectedItem());

            if(PatientInfo.getPatient() == null) {
                throw new NullPointerException();
            }

            Parent inputParent = FXMLLoader.load(getClass().getResource("input_box.fxml"));
            Scene inputScene = new Scene(inputParent);
            Stage inputStage = new Stage();
            inputStage.setScene(inputScene);
            inputStage.initModality(Modality.APPLICATION_MODAL);
            inputStage.setTitle("Edit patient");
            inputStage.getIcons().add(new Image("registerIcon.png"));
            inputStage.showAndWait();

            patientTable.refresh();

            PatientInfo.removeCurrentPatient();
        }
        catch (NullPointerException nullPointerException) {
            showNoPatientSelectedAlert();
        }
        catch (IOException IOException) {
            IOException.printStackTrace();
        }

    }

    /**
     * A method that deletes the selected patient from the tableview
     * @throws NullPointerException if no patient is selected.
     */
    public void deletePatientButtonClicked() throws NullPointerException{
        try {
            Patient selectedPatient = patientTable.getSelectionModel().getSelectedItem();

            Alert deleteConfirmation = new Alert(Alert.AlertType.CONFIRMATION);
            String alertMessage = "Do you really want to delete " + selectedPatient.getFullName() +" from the register?";
            alertBoxLayout(deleteConfirmation, "Delete confirmation", alertMessage, "deleteIcon.png");

            Optional<ButtonType> result = deleteConfirmation.showAndWait();
            if(result.isPresent() && result.get() == ButtonType.OK) {
                patientTable.getItems().remove(selectedPatient);
            }
        }
        catch (NullPointerException nullPointerException) {
            showNoPatientSelectedAlert();
        }
    }

    /**
     * A method that imports patient info from a .csv file and inserts it into the tableview.
     * @throws IOException if unable to reach source
     * @throws IllegalArgumentException if a patient info doesn't follow expexted format
     */
    @FXML
    public void importFromCsv() throws IOException, IllegalArgumentException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose a .csv file");
        fileChooser.setInitialDirectory(new File("src\\main\\resources"));
        File file = fileChooser.showOpenDialog(patientTable.getScene().getWindow());

        if (file != null) {
            if (!file.getName().endsWith(".csv")) {
                Alert invalidFileType = new Alert(AlertType.CONFIRMATION);
                alertBoxLayout(invalidFileType, "Invalid file type", "This filetype is not valid. Please " +
                        "choose a .csv file.", null);

                Optional<ButtonType> response = invalidFileType.showAndWait();
                if (response.isPresent() && response.get() == ButtonType.OK) {
                    importFromCsv();
                }

            }
            else {
                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
                Scanner scanner = new Scanner(reader);
                scanner.useDelimiter(";|\\n");
                boolean oneAlertGiven = false;

                while (scanner.hasNextLine()) {
                    String firstName = scanner.next();
                    String lastName = scanner.next();
                    String generalPractitioner = scanner.next();
                    String socialSecurityNumber = scanner.next();
                    try {
                        if (!firstName.equalsIgnoreCase("firstName")) {
                            Patient patient = new Patient(socialSecurityNumber, firstName, lastName);
                            if(generalPractitioner.equals("null")) {
                                patient.setGeneralPractitioner(null);
                            }
                            else {
                                patient.setGeneralPractitioner(generalPractitioner);
                            }
                            patientTable.getItems().add(patient);
                        }
                    }
                    catch (IllegalArgumentException exception) {
                        if(!oneAlertGiven) {
                            Alert illegalArgumentAlert = new Alert(AlertType.WARNING);
                            alertBoxLayout(illegalArgumentAlert, "Wrong input", "A patient (or patient's)" +
                                    " information in you CVS file is probably written wrong. This is the error message:\n"
                                    + exception.getMessage(), null);
                            Optional<ButtonType> response = illegalArgumentAlert.showAndWait();
                            oneAlertGiven = true;
                        }

                    }
                }
            }
        }

    }

    /**
     * A method that exports patietinfo to a csv file.
     */
    @FXML
    public void exportToCsv() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV (.csv)", "*.csv"));
        fileChooser.setInitialDirectory(new File("src\\main\\resources"));
        fileChooser.setTitle("Export to cvs");
        File file = fileChooser.showSaveDialog(patientTable.getScene().getWindow());
        ObservableList<Patient> patients = patientTable.getItems();

        if(file != null) {
            try {
                FileWriter fileWriter = new FileWriter(file, StandardCharsets.UTF_8);
                fileWriter.write("firstName;lastNam;generalPractitioner;socialSecurityNumber");
                patients.forEach(patient -> {
                    try {
                        fileWriter.write("\n" + patient.getFirstName() + ";" + patient.getLastName() + ";" +
                                patient.getGeneralPractitioner() + ";" + patient.getSocialSecurityNumber());
                    } catch (IOException exception) {
                        exception.printStackTrace();
                    }
                });

                fileWriter.close();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }

    }

    /**
     * A method that closes the stage (and usually the program). //TODO [JavaDoc] More spesific
     */
    @FXML
    private void exitMenuItemClicked(){
        Stage stage = (Stage) menuBar.getScene().getWindow();
        stage.close();
    }

    /**
     * A method that displays an alert box with information about the application.
     */
    @FXML
    private void aboutMenuItemClicked() {
        Alert aboutAlert = new Alert(AlertType.INFORMATION);
        String alertMessage = "Version 1.0\n\nCreated by \nKristina Ødegård";
        alertBoxLayout(aboutAlert, "About Patient Register", alertMessage, "informationIcon.png");
        aboutAlert.setHeaderText("Patient Register application");
        aboutAlert.show();

    }

    /**
     * A method that displays an alert box with contact information about the developer.
     */
    @FXML
    private void contactMenuItemClicked() {
        Alert aboutAlert = new Alert(AlertType.INFORMATION);
        String alertMessage = "Kristina Ødegård\nStudent of Computer Science\nNorwegian University of Science and Technology" +
                "\nTrondheim, Norway\nkrod@stud.ntnu.no";
        alertBoxLayout(aboutAlert, "Developer contact information", alertMessage, "informationIcon.png");
        aboutAlert.setHeaderText("Contact developer");
        aboutAlert.show();
    }

    /**
     * A help method that displays an alert box informing the user that no patient is selected.
     */
    private void showNoPatientSelectedAlert() {
        Alert noPatientSelectedAlert = new Alert(Alert.AlertType.INFORMATION);
        String alertMessage = "No patient selected. Please select a patient and try again.";
        alertBoxLayout(noPatientSelectedAlert, "Error", alertMessage, "questionMarkIcon.png");
        noPatientSelectedAlert.show();
    }

    /**
     * A help method that sets the looks and content of an alert box.
     * @param alert The alert box to be edited
     * @param title The title on the window bar
     * @param message The alert message
     * @param image Image for the upper left window corner, as well as the alert graphic.
     */
    public void alertBoxLayout(Alert alert, String title, String message, String image) {
        alert.setTitle(title);
        alert.setHeaderText(null);
        Stage dialogStage = (Stage) alert.getDialogPane().getScene().getWindow();
        dialogStage.getIcons().add(new Image("registerIcon.png"));
        alert.setContentText(message);

        // Graphic and icon
        if(image != null) {
            Image imageIcon = new Image(image);
            alert.setGraphic(new ImageView(imageIcon));
        }
    }

}
