package ntnu.idatt2001.kristina.mappe.del2;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.*;
import javafx.scene.control.*;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * A controller for the input box (fxml). The input box takes in first and last name, as well as social security number.
 */
public class InputBoxController implements Initializable {

    @FXML private TextField firstNameInput;
    @FXML private TextField lastNameInput;
    @FXML private TextField socialSecurityNumberInput;
    @FXML private Button okButton;
    @FXML private Button cancelButton;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if(PatientInfo.getPatient() != null) {
            setPatientInfo();
        }

    }

    /**
     * A method that can be used both for add new patient and edit patient. It adds the new or updates the patient to
     * the tableview.
     * @throws IllegalArgumentException if the user gives wrong input.
     */
    @FXML
    public void okButtonClicked() throws IllegalArgumentException {
        try {
            String firstName = firstNameInput.getText();
            String lastName = lastNameInput.getText();
            String socialSecurityNumber = socialSecurityNumberInput.getText();

            if (PatientInfo.getPatient() == null) { //This is when the 'Add new patient' button is clicked
                Patient newPatient = new Patient(socialSecurityNumber, firstName, lastName);
                PatientInfo.setPatient(newPatient);
            } else { // This is when the 'Edit patient' button is clicked
                PatientInfo.getPatient().setFirstName(firstName);
                PatientInfo.getPatient().setLastName(lastName);
                PatientInfo.getPatient().setSocialSecurityNumber(socialSecurityNumber);
            }
        } catch (IllegalArgumentException exception) {
            Alert illegalArgumentAlert = new Alert(Alert.AlertType.INFORMATION);
            PrimaryController primaryController = new PrimaryController();
            primaryController.alertBoxLayout(illegalArgumentAlert, "Wrong input", exception.getMessage(), null);
            Optional<ButtonType> response = illegalArgumentAlert.showAndWait();
            if (response.isPresent() && response.get() == ButtonType.OK) {
                illegalArgumentAlert.close();
            }
        }


        Stage stage = (Stage) okButton.getScene().getWindow();
        stage.close();
    }

    /**
     * Removed the current patient from PatientInfo when cancel button clicked.
     */
    @FXML
    public void cancelButtonClicked() {
        firstNameInput.clear();
        lastNameInput.clear();
        socialSecurityNumberInput.clear();
        PatientInfo.removeCurrentPatient();
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }

    /**
     * Sets the info about a selected patient in the InputBox ready to be edited.
     */
    public void setPatientInfo() {
        firstNameInput.setText(PatientInfo.getPatient().getFirstName());
        lastNameInput.setText(PatientInfo.getPatient().getLastName());
        socialSecurityNumberInput.setText(PatientInfo.getPatient().getSocialSecurityNumber());
    }

}
