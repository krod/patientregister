module ntnu.idatt2001.kristina.mappe.del2 {
    requires javafx.controls;
    requires javafx.fxml;

    opens ntnu.idatt2001.kristina.mappe.del2 to javafx.fxml;
    exports ntnu.idatt2001.kristina.mappe.del2;
}