package ntnu.idatt2001.kristina.mappe.del2;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PatientRegisterTest {

    // set socialsecurity number with more than 11 characters
    @Nested
    class CreatingPatients {
        @Test
        @DisplayName("Patient with too long social security number not created")
        void patientWithTooLongSocialSecurityNumberNotCreated() {
            assertThrows(IllegalArgumentException.class, ()-> new Patient("11111122222434",
                    "Helene", "Harefrøken"), "Too many digits in social security number should throw");
        }

        @Test
        @DisplayName("Patient with number in last name not created")
        void patientNumberInLastNameNotCreated() {
            assertThrows(IllegalArgumentException.class, ()-> new Patient("01234567890",
                    "Markus", "Bakk1"), "Digit in last name should throw");
        }
    }
}
